### Deep Cuts
This game was originally intended as an AdvXJam game submission for 2020, whose theme was "Having a Good Time". I ended up
going way over the deadline, but this also allowed me to flesh out the game mechanics and design work far beyond what was
originally expected. This project is still in development, not even close to being done, but there's a lot of cool stuff
in it now that I would've never been able to do prior to working on this game.

Update: this project has had a few different names over the years. It started as
"Euphoria", regarding the endless pursuit of trying to be happy when struggling
against baggage, depression, and anxiety. Unfortunately, that name was already
taken by a somewhat graphic visual novel. I tried naming it "Life Is a Party",
but it never really carried the emotional weight beneath the surface.

"Deep Cuts" is a double entendre, a phrase with two inferred meanings. A big
portion of the game involves collecting special pieces of knowledge regarding
music, pop culture, and entertainment. A beloved underrated album or song is
sometimes referred to as "A Deep Cut". Conversely, it also refers to the
lingering emotional pain sometimes carried over the course of months, years, or
even decades. I feel that this contrast reflects well on the game's themes.

### Special Thanks, Credits Due

I want to give a shout-out to the following people for their fantastic module work,
which I used liberally for the development of this game. It is humbling to get so much help
from people in the community who are so much smarter than me when it comes to the deep foundations
of the engine:

* [Edmundito](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=45), for his fabulous [AGS Tween module](https://www.adventuregamestudio.co.uk/forums/index.php?topic=57315.0). This is what helps give the interface
and camera movements an extra level of momtion smoothness
* [Crimson Wizard](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=8413), who developed [an improved AGS timer system](https://www.adventuregamestudio.co.uk/forums/index.php?topic=55545.msg636576559). The native one is a real
pain to work with when it comes to repetitive non-blocking functions, and I needed it
for a couple of linear functions such as the Anxiety levels gradually decreasing.
* [Abstauber](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=8831), for his absolutely phenomenal [CustomDialogGUI module](https://www.adventuregamestudio.co.uk/forums/index.php?topic=36313.0). I had a really
hard time using drawing functions and dynamic sprites to render a "fake" GUI. thanks
to his work, I was able to create a dialog system that incorporates buttons
with images in them for dialog.

Also, special thanks to [Khris](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=2754) and [Crimson Wizard](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=8413) for endlessly answering all of my contrived questions.

#### Basic Premise
The AdvXJam theme for this year (2020) is "Having a Good Time", and the
interpretation for entries is very permissive. So, I'm going to unload some of
my personal struggles in trying to have a good time, trying to get along with
other people, and the baggage that crops up over years and years.

You play Paul, an introvert with anxiety problems who gets a call from an
old friend to go to their party. You've drifted apart over the course of a
decade, and you really want to impress him and everyone there.

So, you're gonna fake it until you make it!

You're at a party with a bunch of ultra-hipsters (at least, you think they're
ultra-hipsters), and you don't know a single thing about what they like. This
is a game of keeping up appearances, where you try to impress people by bluffing.
You have access to fragments of information to keep the game going, but
occasionally you have to just wing it and choose something at random.

Correct answers reveal more fragments, allowing you to advance further.
Incorrect answers spike up your anxiety, creating a feedback loop.

You can reduce anxiety by drinking alcohol or ingesting other substances
at the party...but whatever you put in your body puts your faculties at risk.
How many lies are you keeping track of?
