# ags-game-ci-utils
Utility scripts for using Continuous Integrations with Adventure Game Studio games.

See [Dungeon Hands](https://github.com/ericoporto/DungeonHands) repository for an example on how to use it.

## `appveyor.yml` example

```
version: 1.0.{build}
image: Visual Studio 2015
init:
  - ps: iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
install:
  - cd %APPVEYOR_BUILD_FOLDER%
  - git submodule update --init --recursive
build_script:
  - call ags-game-ci-utils/installAGS.cmd
  - call ags-game-ci-utils/buildAGSGame.cmd
  - call ags-game-ci-utils/packageArtifacts.cmd
artifacts:
  - path: Compiled/Windows/DungeonHands-windows.zip
    name: DungeonHands-windows.zip
  - path: Compiled/Linux/DungeonHands-linux.tar.gz
    name: DungeonHands-linux.tar.gz
```

## `azure-pipelines.yml` example

```
trigger:
- master
pool:
  vmImage: 'vs2015-win2012r2'
steps:
- checkout: self  # self represents the repo where the initial Pipelines YAML file was found
  submodules: recursive 
- task: BatchScript@1
  inputs:
    filename: ags-game-ci-utils/installAGS.cmd
- task: BatchScript@1
  inputs:
    filename: ags-game-ci-utils/buildAGSGame.cmd
    arguments: $(Build.SourcesDirectory)
- task: BatchScript@1
  inputs:
    filename: ags-game-ci-utils/packageArtifacts.cmd
    arguments: $(Build.SourcesDirectory)
```
