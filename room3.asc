// room script file

function region2_WalksOnto()
{
  region[2].Enabled = false;
  region[1].Enabled = true;
  cEgo.Walk(cEgo.x, 230, eBlock, eAnywhere);
  cEgo.ChangeRoom(1,  180,  140, eDirectionDown);
}

function region1_WalksOnto()
{
  region[1].Enabled = false;
  region[2].Enabled = true;
}

function region3_WalksOnto()
{
  cEgo.Walk(300, cEgo.y, eBlock, eAnywhere);
  cEgo.ChangeRoom(4,  160,  150);
}

function region4_WalksOnto()
{
  cEgo.Walk(14, cEgo.y, eBlock, eAnywhere);
  cEgo.ChangeRoom(5,  277,  162);
  region[4].Enabled = false;
}

function region5_WalksOnto()
{
  region[4].Enabled = true;
}
