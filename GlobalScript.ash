// Main header script - this will be included into every script in
// the game (local and global). Do not place functions here; rather,
// place import definitions and #define names here to be used by all
// scripts. 
 import function openJournal(InventoryItem* entry);
 import function examineItem(InventoryItem* item);
 import function checkConfidence();
 import function lowerConfidence(float change);
 import function raiseConfidence(float change);
 import function getAnxiety();
 import function fakeAnimation();
 import function increaseAnxiety(int AnxietyUp);
 import function decreaseAnxiety(int AnxietyDown);
 import function Notify(String headline,  String description, String type);
 import function meetChar(this Character*);
 import function incomingCall(int call);
 import function ignoreCall(int call);
 import function getOpinion(this Character*);
 import function raiseOpinion(this Character*,  int opinionUp);
 import function lowerOpinion(this Character*,  int opinionDown);
 import function coolDown();
 import function changeOutfit(this Character*,  String outfit);
 import function speechMode(String mode);
 import function Portrait(this Character*);
 import function laTalk(this Character*) ;
 import function bump();
 import function openMap();
 import function closeMap();
 import function checkController();
 import function startController();
 import function chooseTopic();
 import function setFilter(this Character*);
 import function discoverThing(this Character*, InventoryItem* thing);