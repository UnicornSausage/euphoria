// new module header
 import function getAction();
 import function on_event(EventType event, int data);
 import function setLabel();
 import function setContext();
 import function setPointer();
 import void AdjustActionBarPosition();
 import function setInvPos();
 import function closeInvWindow();
 import function invLoc();
 import function invHover();
 