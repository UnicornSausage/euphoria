// room script file

function room_AfterFadeIn()
{
  introMusic.Stop();
  anxiety_rate = 5.0;
  SetScreenTransition(eTransitionInstant);
}

function room_Load()
{
}

function room_FirstLoad()
{
  cSteve.SpeechView = STEVESHADOW;
  cSteve.Transparency = 100;
  incomingCall(0);
}

function oPillBottle_Look()
{
  Display("Those are your anxiety meds.");
}

function oPillBottle_Interact()
{
  Display("You pick up the pill bottle.");
  oPillBottle.Visible = false;
  cEgo.AddInventory(iMeds);
}

function oGoosebumps_Look()
{
  Display("A few Goosebumps books are piled up on your desk.");
}

function oGoosebumps_Interact()
{
  Display("You debate thumbing through them, but you've read both Revenge of the Mutant and One Day at Horrorland a million times.");
}

function oGoosebumps_Talk()
{
  cEgo.Say("Yeah, yeah. I'm a grown man that still reads Goosebumps.");
}

function iMac_Look()
{
  Display("Yep. It's a bondi blue PowerPC iMac. For some reason, you have one, even though it's 2020.");
}

function iMac_Talk()
{
// Animate the anxiety meter independent of blocking
      if (anxiety_level == 0) {
          cEgo.Say("Aw, man, this thing is awesome! I modified it to run MorphOS, since it has a PowerPC chip.");
      }
        else if (anxiety_level == 1) {
            cEgo.Say("Hey, don't judge me. I needed something capable of running MorphOS.");
        }
        
          else if (anxiety_level == 2) {
          cEgo.Say("You know, at least it was cheap...");
        }
        
          else if (anxiety_level == 3) {
          cEgo.Say("You know, it's getting harder and harder to support this thing...");
        }
          else if (anxiety_level == 4) {
          cEgo.Say("I should probably just fucking put Linux on it, honestly.");
        }
          else if (anxiety_level == 5) {
          cEgo.Say("Why do I waste my time with this broken piece of shit?");
        }
          else if (anxiety_level == 6) {
          cEgo.Say("Fuck this awful computer, and fuck me for being dumb enough to buy into some piece of shit hipster operating system.");
        }     
}

function oPillBottle_Talk()
{
          // Animate the anxiety meter independent of blocking
      if (anxiety_level == 0) {
          cEgo.Say("You know, I'm not even sure I need these.");
      }
        else if (anxiety_level == 1) {
            cEgo.Say("They help every now and then.");
        }
        
          else if (anxiety_level == 2) {
          cEgo.Say("I wish I didn't have to take these...");
        }
        
          else if (anxiety_level == 3) {
          cEgo.Say("Sigh...");
        }
          else if (anxiety_level == 4) {
          cEgo.Say("I'm probably going to be on this fucking medication for the rest of my life.");
        }
          else if (anxiety_level == 5) {
          cEgo.Say("Fuck my life! I hate these fucking pills!");
        }
          else if (anxiety_level == 6) {
          cEgo.Say("FUCK! I FEEL FUCKING HORRIBLE!");
        }      
}

function region2_WalksOnto()
{
  cEgo.Walk(122, 109, eBlock, eAnywhere);
  cEgo.ChangeRoom(7,  158,  123, eDirectionDown);
}

function room_Leave()
{
  anxiety_rate = 30.0;
  comingFrom = 2;
}

function hMouse_Interact()
{
  cEgo.Say("I'd rather not use it, if I don't have to...");
}

function hMouse_Look()
{
  cEgo.Say("God-awful hockey-puck mouse...");
  cEgo.Say("I keep meaning to get it replaced, but I guess it's not that important.");
}

function hPillow_Look()
{
   cEgo.Say("It looks comfy as heck.");
}

function hPillow_Interact()
{
  cEgo.Say("I dunno...if I set my head down on it, I'm at high risk for taking a nap.");
}

function hPillow_Talk()
{
  cEgo.FaceDirection(eDirectionDown);
  cEgo.Say("Uh...let's skip the pillow talk for now.");
}

function hBed_Look()
{
  cEgo.Say("California King size, baby!");
}

function hBed_Interact()
{
  cEgo.Say("It's pretty fun to dive into.");
  cEgo.Say("But...we've got things to do right now.");
}

function hBed_Talk()
{
  cEgo.Say("I love you, mattress.");
}

function hPoster2_Look()
{
  cEgo.FaceLocation(105, 85);
  cEgo.FaceDirection(eDirectionDown);
  cEgo.Say("That's god.");
}

function hPoster2_Interact()
{
  cEgo.Say("Nah, let's leave Freddie alone for right now.");
}

function hPoster2_Talk()
{
  cEgo.FaceLocation(105, 85);
  cEgo.Say("Dear Freddie...");
  cEgo.Say("Thank you for all the wonderful music.");
}

function hHallway_Interact()
{
  cEgo.FaceDirection(eDirectionDown);
  cEgo.Say("Why do you want to interact with the hallway so bad? Once the programmer adds a door in here, you shouldn't be able to even interact with it...");
}

function hHallway_Look()
{
  cEgo.Say("That's, uh, the hallway outside of my room.");
}

function hHallway_Talk()
{
  cEgo.Say("Hello?!");
  Wait(40);
  Display("There is no answer.");
}

function hWindow_Look()
{
  cEgo.Say("Yep, that's Peoria Skyline.");
}

function hWindow_Interact()
{
  cEgo.Say("It's actually kind of a hassle to open.");
}

function hWindow_Talk()
{
  chooseTopic();
}
