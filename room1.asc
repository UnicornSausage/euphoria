// room script file

function room_FirstLoad()
{
  Game.Camera.TweenPosition(4.0, 0, 0, eEaseInBackTween, eBlockTween);
  Game.Camera.AutoTracking = true;
  cEgo.Say("Wow. What a party...");
  Wait(20);
  cEgo.Say("Sure are a lot of people here...");
  increaseAnxiety(1);
  firstTimeAtParty = false;
  cJoe.Walk(128,  171, eBlock, eWalkableAreas);
  cJoe.FaceCharacter(cEgo, eBlock);
  cJoe.Say("Hey, welcome to the party!");
  cEgo.Say("Hey, nice to meet you, I'm Steve's friend Paul.");
  cJoe.Say("Oh, nice to meet you!");
  cJoe.Say("Steve should be in his bedroom in the back.");
  cJoe.Say("Door on the left at the end of the hallway, you can't miss it!");
  cEgo.Say("Thanks!");
  cJoe.Walk(477,  166, eNoBlock, eWalkableAreas);
}

function room_Load()
{
  if  (firstTimeAtParty == true) {
  Game.Camera.SetAt(638, 174);
  cSteve.ChangeRoom(5, 140,  160, eDirectionDown);
  cSteve.Transparency = 0;
  cSteve.SpeechView = 3;
  }
  
}

function region1_WalksOnto()
{
  cEgo.Walk(171,  121, eBlock, eWalkableAreas);
  cEgo.ChangeRoom(3,  160, 190);
}

function region3_WalksOnto()
{
  cEgo.Walk(293,  150, eBlock, eWalkableAreas);
  cEgo.ChangeRoom(6,  200, 156, eDirectionDown);
}

function region2_WalksOnto()
{
  cEgo.Walk(263,  141, eBlock, eWalkableAreas);
  cEgo.ChangeRoom(6,  252, 164, eDirectionLeft);
}
